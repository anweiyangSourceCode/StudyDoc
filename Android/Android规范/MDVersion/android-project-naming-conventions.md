---
id: android-project-naming-conventions
title: 代码规范
---
不以规矩，不能成方圆，好的代码风格在提高团队的开发效率，降低沟通成本，降低协作成本方面具有难以估量的作用，所以好的、可执行的代码规范对于团队开发来讲意义重大。

代码规范原则如下：  
**命名：见名知意。**  
**结构：易于理解，可读性第一。**

---
## Java代码编码规范
### 十条基本规范
1. 【强制】可读性第一原则。书写代码时，代码的可读性大于代码的简洁性。可读性第一，不要盲目追求代码的简洁，在团队协作中可读性好的代码才有更好的可维护性，而且一般情况下也具有更好的健壮性。代码的语法运用与书写顺序应以可读性优先。

2. 【强制】逻辑复杂、难以理解的代码必须加入注释，比如：复杂的业务逻辑、算法、控制流程等。注释的内容需要写清楚代码的功能，如果有必要还要写明为什么这么做，以便于后续维护。

3. 【强制】代码中的命名严禁使用拼音与英文混合的方式，更不允许直接使用中文的方式。  
说明：正确的英文拼写和语法可以让阅读者易于理解，避免歧义。注意，即使纯拼音命名方式 也要避免采用。  
<blockquote class="success">正例：beijing / hangzhou 等国际通用的名称，可视同英文。</blockquote>
<blockquote class="danger">反例：DaZhePromotion [打折] / getPingfenByName() [评分] / int 某变量 = 3 </blockquote>

4. 【强制】类名、方法名的命名要做到望文知意，为了达到代码自解释的目标，任何自定义编程元素在命名时，使完整的单词组合来表达其意，不要害怕名字过长。类名需要用名词表述、方法名需要按照功能进行表述。  
<blockquote class="success">正例：int orderCount;</blockquote>
<blockquote class="danger">反例：int a;  —— 随意命名方式，不可取。</blockquote>

5. 类、方法、参数、变量的命名  
【强制】类名使用 UpperCamelCase 风格，必须遵从驼峰形式  
<blockquote class="success">正例:MarcoPolo / UserDO / XmlService / TcpUdpDeal / TaPromotion </blockquote>
<blockquote class="danger">反例:macroPolo / UserDo / XMLService / TCPUDPDeal / TAPromotion </blockquote>
【强制】方法名、参数名、成员变量、局部变量都统一使用 lowerCamelCase 风格，必须遵从 驼峰形式。  
<blockquote class="success">正例: localValue / getHttpMessage() / inputUserId</blockquote>
【强制】常量命名全部大写，单词间用下划线隔开，力求语义表达完整清楚，不要嫌名字长。  
<blockquote class="success">正例:MAX_STOCK_COUNT</blockquote>
<blockquote class="danger">反例:MAX_COUNT</blockquote>

6. 【强制】包名统一使用小写，点分隔符之间有且仅有一个自然语义的英语单词。包名统一使用 单数形式，但是类名如果有复数含义，类名可以使用复数形式。  
`例：应用工具类包名为 com.open.util、类名为 MessageUtils`

7. 【强制】单行字符数限制不超过 120 个，超出需要换行，换行时遵循如下原则：
  - 第二行相对第一行缩进 4 个空格，从第三行开始，不再继续缩进，参考示例。
  - 运算符与下文一起换行。
  - 方法调用的点符号与下文一起换行。
  - 方法调用时，多个参数，需要换行时，在逗号后进行。
  - 在括号前不要换行，见反例。  

  <blockquote class="success">正例</blockquote>

  ```java
    StringBuffer sb = new StringBuffer(); 
    // 超过 120 个字符的情况下,换行缩进 4 个空格,点号和方法名称一起换行 
    sb.append("zi").append("xin")... 
        .append("huang")...
        .append("huang")...
        .append("huang");

  ```
  <blockquote class="danger">反例</blockquote>

  ```java
    StringBuffer sb = new StringBuffer(); 
    // 超过 120 个字符的情况下,不要在括号前换行 
    sb.append("zi").append("xin")...append
        ("huang"); 
    // 参数很多的方法调用可能超过 120 个字符,不要在逗号前换行 
    method(args1, args2, args3, ... 
        , argsX);

  ```

8. 【强制】避免通过一个类的对象引用访问此类的静态变量或静态方法，无谓增加编译器解析成本，直接用类名来访问即可。

9. 【强制】在 if/else/for/while/do 语句中必须使用大括号。即使只有一行代码，避免采用单行的编码方式：  
<blockquote class="success">正例：if (condition) { statements; }</blockquote>
<blockquote class="danger">反例：if (condition) statements;</blockquote>

10. 【强制】在一个 switch 块内，每个 case 要么通过 break/return 等来终止，要么注释说明程 序将继续执行到哪一个 case 为止;在一个 switch 块内，都必须包含一个 default 语句并且放在最后，即使它什么代码也没有。

> 参考资料：[《阿里巴巴Java开发手册》](https://yq.aliyun.com/attachment/download/?id=2023)

---

## xml布局文件编码规范

### 命名规范
#### id 命名规范  
控件缩写_业务规则  
例：`et_user_name`、`btn_confirm`、`btn_reload`

#### 控件缩写
  为了方便控件 id 的命名，我们约定一些常用控件的缩写，控件缩写采用控件类名驼峰标识的首字母组合。  
  常用控件缩写：  

|控件名称|缩写名称|
|-|-|  
|TextView|tv|
|EditText|et|
|Button|btn（特殊）|
|ImageView|iv|
|ProgressBar|pb|
|ListView|lv|
|GridView|gv|
|ScrollView|sv|
|LinearLayout|ll|
|RelativeLayout|rl|
|FrameLayout|fl|
|RecyclerView|rv|

### 属性排列
规范布局控件的属性排列顺序，可以提高布局代码的可读性，维护性，提高团队协作的效率。排列顺序如下：  
**1\. id**   
    （`id`）  
**2\. 宽高**  
    （`layout_width`、`layout_height`）  
**3\. 内外边距**  
    （`layout_paddingTop`、`layout_paddingBottom`、`layout_paddingLeft`、`layout_paddingRight`、`layout_marginTop`、`layout_marginBottom`、`layout_marginLeft`、`layout_marginRight`）  
**4\. 位置**  
    （`layout_above`、`layout_below`、`layout_toLeftOf`、`layout_toRgithOf`、`layout_gravity`、`layout_centerVertical`、`layout_centerInParent`、`layout_centerHorizontal` 等）  
**5\. 其他**  
    例： 
  - 文字相关
      （`text`、`textColor`、`textSize`）
  - 背景、图片资源
      （`background`、`src`）
  - 图片的缩放策略、文字的单行显示、超字数省略等等
      （`scaleType`、`singleLine`、`ellipsize` 等）  

**6\. 可见性**（`visibility`）  
    例：  
  ```xml
  <Button
    android:id="@+id/btn_reload"
    android:layout_width="100dp"
    android:layout_height="30dp"
    android:layout_marginTop="@dimen/common_margin_top"
    android:layout_below="@id/tv_blank_sub_title"
    android:layout_centerHorizontal="true"
    android:layout_centerVertical="true"
    android:text="@string/blankpage_net_work_btn_txt"
    android:textSize="@dimen/common_font_size_15"
    android:textColor="@color/common_warn_assist_font_color"
    android:background="@drawable/selector_btn_common_reload"
    android:gravity="center"
    android:visibility="gone"/>
  ```
