---
id: android-project-style-conventions
title: 样式规范
---

大家在开发中是否经历过下面的场景：有这样两个画面 X 与 Y ，它们都有搜索功能，UI 设计与交互完全相同，交给程序员 A 与 B 进行开发，最后虽然二者开发的画面的表现与交互完全相同，但是却是完全不同的实现。过了一段时间 C 在去维护这两个看似一样的东西时，却遇到了阻力，用了双倍于计划的时间。

不同的人员开发类似的功能，虽然最终效果几乎相同，但是具体的实现可能完全不同。带来的后果就是：为维护带来了理解鸿沟，同时每个人都单独造一遍轮子，对于项目来说也是一种资源的浪费。

**所以，我们需要通过样式规范来解决 UI 层面上的代码重复与风格不统一的问题。**

> 注意，我们要提供的是一组规则，一组在 Android 开发中如何组织与使用画面颜色、间距、样式的规则，而不是具体的颜色与样式的设计方法。那些是 App 的 UI 设计人员应该关心的，请参照设计规范部分。

---
## 颜色
#### 分类
按照颜色的用途，分为**字体颜色**与**非字体颜色**。  
按照颜色的使用范围，分为**共通颜色**与**非共通颜色**。  
将上述条件进行组合，我们得到工程中的颜色分类，为如下四种：  
- 共通字体颜色
- 非共通字体颜色
- 共通颜色
- 非共通颜色

| |字体|非字体|
|-|-|-|  
|共通|共通字体颜色|共通颜色|
|非共通|非共通字体颜色|非共通颜色|

#### 组织  
所有的颜色都要抽出到 `colors.xml` 中统一管理
#### 命名  
共通颜色需要以 `common_` 开头、字体颜色需要以 `_font_color` 结尾。
- 共通字体颜色  
    以 `common_` 开头，以 `_font_color` 结尾，中间为该颜色变量的表意  
    例：`common_warning_font_color`
- 非共通字体颜色  
    以 `模块名称_` 开头，以 `_font_color` 结尾，中间为该颜色变量的表意  
    例：`register_next_step_btn_font_color`
- 共通颜色  
    以 `common_` 开头，以 `_color` 结尾，中间为该颜色变量的表意  
    例：`common_warning_bg_color`
- 非共通颜色  
    以 `模块名称_` 开头，以 `_color` 结尾，中间为该颜色变量的表意  
    例：`register_next_step_btn_bg_color`

> 注：当共通颜色是一个没有业务意义的常见的纯色时，可以省略 `_color` 后缀，例如：`common_white`、`common_black`，但是这样表示的颜色必须与其名称完全相符，也就是说，如果一个 color 变量的名称是 `common_white` 则该变量的值必须是 `#ffffff`，而不能是其他的色值。

#### 举例（实施后贴出）

---

## 间距、字号
### 间距
#### 分类  
按照间距的使用范围，将间距分为**共通间距**与**非共通间距**。  
- 共通间距：贯穿整个 App 的全部画面（如，共通内边距、外边距）或在 App 的多个画面中出现频率较高的间距。  
- 非共通间距：只在特定画面上出现一次或多次，在特定画面外不出现的间距。

#### 组织
- 所有的共通间距都要抽出到 `dimens.xml` 中统一管理。
- 只在特定画面上出现一次的非共通间距，可以抽出，也可以不抽出，不做硬性要求。

#### 命名  
- 共通间距需要以 `common_` 开头，表示高度时，需要以 `_height` 结尾，表示宽度时，需要以 `_width` 结尾。  
例：`common_list_item_height`  
- 非共通间距需要以 `模块名称_` 开头，表示高度时，需要以 `_height` 结尾，表示宽度时，需要以 `_width` 结尾。  
例：`profile_user_avatar_width`

### 字号
#### 分类  
按照 UI 设计是否定义了字号的使用场景，将字号分为**普通字号**与**层级字号**。  
- 普通字号：在 UI 设计中未明确定义在整个 App 中的使用场景的字号。  
- 层级字号：在 UI 设计中明确定义了在整个 App 中的各种使用场景（如：画面标题、列表标题等）的字号。

#### 组织
- 所有的字号都要抽出到 `dimens.xml` 中统一管理。
- 字号的单位统一使用 `sp` 。
> 注：字号单位 `sp` 与 距离单位 `dp` 的区别主要在于，`sp` 除了受屏幕密度影响外,还受到用户的字体大小影响，所以通常情况下，建议使用 `sp` 设定字体大小。

#### 命名
- 普通字号统一以 `common_font_size_字号` 的方式命名  
例：`common_font_size_18`
- 层级字号统一以 `common_font_size_level_层级号` 的方式命名  
例：`common_font_size_level_1`

#### 举例：
```xml
    <!--普通字号-->
    <dimen name="common_font_size_23">23sp</dimen>
    <dimen name="common_font_size_21">21sp</dimen>
    <dimen name="common_font_size_20">20sp</dimen>
    <dimen name="common_font_size_19">19sp</dimen>
    <dimen name="common_font_size_18">18sp</dimen>
    <dimen name="common_font_size_17">17sp</dimen>
    <dimen name="common_font_size_16">16sp</dimen>
    <dimen name="common_font_size_15">15sp</dimen>
    <dimen name="common_font_size_14">14sp</dimen>
    <dimen name="common_font_size_13">13sp</dimen>
    <dimen name="common_font_size_12">12sp</dimen>
    <dimen name="common_font_size_11">11sp</dimen>
    <dimen name="common_font_size_10">10sp</dimen>

    <!--层级字号-->
    <dimen name="common_font_size_level_1">18sp</dimen>
    <dimen name="common_font_size_level_2">16sp</dimen>
    <dimen name="common_font_size_level_3">14sp</dimen>
    <dimen name="common_font_size_level_4">12sp</dimen>
```


---

## 样式
### 样式的作用
样式，是一组画面元素属性的集合，定义画面的元素如何显示。  
使用样式可以帮助我们减少布局文件中的代码，更好的完成统一画面风格、快速变换主题等需求。

> 在 Android 开发中，在 `styles.xml` 中定义 App 中用到的样式。作为 App 的开发者，你可以为每个控件定义样式，并将其应用于你希望的任意多的页面中。如需进行全局的更新，只需简单地改变样式，然后 App 中的所有指定了该样式的元素均会进行相应的更新。

### 抽出原则
相同控件的同一份属性集合在同一个页面反复出现多次（通常为 4 次及以上）或者在不同画面反复出现多次，则需抽出为样式。

#### 分类
按照抽出样式的使用范围，分为**共通样式**与**非共通样式**。  
- 共通样式：在 App 的不同模块的多个画面中出现频率较高的样式。  
- 非共通样式：只在特定画面上出现多次或在同一模块的不通过画面中出现多次的样式。

#### 组织
所有的样式都要抽出到 `styles.xml` 中统一管理。

#### 命名
- 所有的样式都要采用大驼峰命名法，必要时以 “.” 点号分隔。
- 共通样式需要在名称前加入 `Common.` 前缀。  
例：`Common.Button.Primary`
- 非共通样式需要在名称前加入 `模块名.` 前缀。  
例：`Order.Button.Primary`

### 举例（实施后贴出）
共通系统按钮样式  
共通简单列表项样式（带icon、不带icon、带箭头、不带箭头）  
主题对话框样式  
共通输入框样式  
共通标题栏样式  
共通水平分割线



