---
id: android-project-structure
title: 工程目录
---

好的目录划分应满足**分类明确**、**容易查找**的目标。  
在这个前提之下，结合工程开发中的实际情况，我们在划分目录时采用**先分模块**、**再分目录**的方法，在文件命名上遵循**见名知意**的原则。  
> 先分模块，后分目录：先将模块划分为非功能模块（业务无关）与功能模块（业务相关）、非功能模块单独划分目录，功能模块则根据具体的业务进行划分，同一类业务划分为同一个模块，组成一个目录。

---

## 目录结构

```java
android-project
  src
    ├──main
       ├── AndroidManifest.xml
       ├── java-com-app-school  
       │   ├── module
       │   │   ├── base
       │   │   ├── auth（可选）
       │   │   └── ……
       │   ├── util
       │   │   ├── FileUtils.java (可选)
       │   │   ├── JsonUtils.java (可选)
       │   │   ├── DateUtils.java (可选)
       │   │   ├── ImageUtils.java (可选)
       │   │   └── ……
       │   ├── network
       │   │   ├── base（可选）
       │   │   ├── ReqUserSignIn.java （可选）
       │   │   └── ……
       │   ├── database（可选）
       │   ├── ui（可选）
       │   ├── constant
       │   └── model（可选）
       ├── assets
       ├── res
       │   ├── anim
       │   ├── drawable
       │   │   ├── shape_common_btn_dialog_cancel_normal.xml（可选）
       │   │   └── ……
       │   ├── layout
       │   │   ├── setting_act.xml（可选）
       │   │   └── ……
       │   ├── mipmap
       │   │   ├── common_ic_arrow_left_white.png（可选）
       │   │   ├── ic_user_info_edit.png（可选）
       │   │   ├── bg_startup_bottom.png（可选）
       │   │   └── ……
       │   └── values
       │       ├── colors.xml
       │       ├── dimens.xml
       │       ├── strings.xml
       │       ├── styles.xml
       │       ├── strings_array.xml
       │       └── attrs.xml
    ......
```
> 注：上图中的目录结构以 AndroidStudio 中的典型目录结构为准，而且并没有对一个Android工程中的所有目录进行一一例举，只选择我们关心的开发代码目录进行说明。

如上，现对目录划分进行说明：
- `src/main/包路径/module/**`  功能模块目录，用于编写业务逻辑模块代码。
- `src/main/包路径/util/**`  系统工具目录，用于编写程序中用到的工具类，比如：FileUtils.java（文件操作工具类）、JsonUtils.java（Json处理工具类）、DateUtils.java（日期处理工具类）、ImageUtils.java（图片处理工具类）等。
- `src/main/包路径/network/**`  网络通信目录，用于编写程序中与网络通信相关的代码。
- `src/main/包路径/database/**`  数据库代码目录，用于编写程序中与本地数据库操作相关的代码。
- `src/main/包路径/ui/**`  视图组件目录，用于编写自定义视图或者存放第三方视图。
- `src/main/包路径/constant/**`  常量目录，用于存放系统中用到的常量。
- `src/main/包路径/model/**`  数据模型目录，用于编写系统中数据模型相关的代码（比如用到的数据模型类以及数据模型类的生成类、操作类）。
- `src/main/assets/**`  用于存放需要打包到应用程序的静态文件，以便部署到设备中。注：与res/raw不同，assets支持任意深度的子目录。这些文件不会生成任何资源ID，必须使用/assets开始（不包含它）的相对路径名。
- `src/main/res/anim/**`  用于存放动画资源文件。
- `src/main/res/drawable/**`  用于存放xml绘画资源文件。
- `src/main/res/layout/**`  用于存放视图布局资源文件。
- `src/main/res/mipmap/**`  用于存放图片资源文件。
- `src/main/res/value/**`  用于存放程序静态变量资源文件，比如：静态字符串、颜色、间距变量值等。

---

## 文件命名

### 命名原则
命名原则：见名知意。
### 实施规范
#### Java文件
- Java文件名采用大驼峰命名法。
- Activity、Fragment、Service、Receiver、Adapter、Listener 类型的类文件名末尾相应的需要以 Activity、Fragment、Service、Receiver、Adapter、Listener结尾。
- 类的命名用名词，当类名由多个单词组成时，同一类别业务的类使用同一前缀。

#### xml文件
- 文件名全部小写，多个单词用下划线分隔。
- 布局文件（layout 文件夹下）  
  - 文件命名以模块划分，activity 与 fragment 的布局文件后面加入 act、frag 后缀  
<blockquote class="success">正例：setting_act.xml</blockquote>  
<blockquote class="danger">反例：activity_setting.xml</blockquote>  
> 注：之所以不推荐 `activity_setting.xml` 这种命名方式，是因为一旦采用这种方式，相同模块的布局文件，在目录中的排列会因为文件命名而分散到各处，比如 `SettingActivity` 画面中用到了两个布局文件，画面整体的布局和页面中的列表条目的布局，推荐如下的命名方式：`setting_act.xml`、`setting_act_lvi.xml`，这样的命名方式在集成开发环境中这两个文件会被排列在一起。如果采用如下方式的命名：`activity_setting.xml`、`item_list_setting.xml`，则在集成开发环境中两个文件就会被排列在距离较远的位置，不利于查找。
  - 布局文件可以使用约定的缩写以减小文件名称长度  
  常用布局文件名称缩写约定：  
  `activity -> act`   
  `fragment -> frag`  
  `list_view_item -> lvi`  
  `grid_view_item -> gvi`  

- 绘画文件（drawable 文件夹下）   
  文件命名以功能为前缀命名，表示这个绘画文件表示一个什么样的资源（本资源是形状？选择器？图标？等等），功能前缀后面加入表示该资源对应的逻辑名称。  
  常用前缀：`shape`（形状）、`selector`（选择器）、`icon`（图标）  
  例：  
  `shape_common_btn_dialog_cancel_normal.xml` —— 本资源为形状  
  `selector_btn_round_normal.xml` —— 本资源为选择器  
  `icon_common_loading.xml` —— 本资源为图标  
 
  > 注：
  - 用 `shape`、`selector`、`icon` 等做前缀的好处是，我们可以通过文件名一眼看出这个 drawable 的作用是什么，有利于在面对一些画面UI问题时帮助我们快速分析问题（比如某按钮按下时没有样式变化一类的问题）。
  - 之所以不把业务名称写在文件名前面，是因为在工程中往往有很多 drawable 资源是可以复用的，这时如果用业务来命名，会导致资源文件泛滥，比如登录画面中的确定按钮与支付画面中的确定按钮都需要用到一个相同的 selector，由于按照业务名称命名，我们不得不写两个一模一样的 selector 来对应代码的逻辑，`login_btn_selector.xml`，`payment_btn_selector.xml`，其实我们只需要写一个 selector，并给这个 selector 起一个泛化的名称，比如：`selector_common_primary_btn.xml`，然后为两个画面的按钮都指定此 selector 即可。所以，泛化可复用的 drawable 的命名，并复用之。当然，对于程序中没有复用或者需要特殊处理的 drawable 除外。

#### 图片文件
  - 文件名全部小写，多个单词用下划线分隔。
  - 通用图片加入 `common` 前缀，图标类图片加入 `ic` 前缀，背景类图片加入 `bg` 前缀，前缀后面名称按照图片表示的业务逻辑命名。
  > 注：图片资源加入前缀的目的主要是方便图片的查找与对图片的理解，例如，工程中被很多地方共用的一些图片，加入 `common` 前缀可以有效的防止一个图片被以不同的名称放入资源文件夹多次的问题。加入 `ic` 与 `bg` 前缀可以使我们快速的知道这个图片是一个图标类的小型图片还是一个背景类的大型图片。  

  例：  
  `common_ic_arrow_left_white.png` —— 通用类图标，左向箭头图标  
  `ic_user_info_edit.png` —— 非通用类图标，用户信息编辑图标  
  `bg_startup_bottom.png` —— 非通用类图片，启动画面底部背景图片

#### 资源文件（二进制文件、html、js等等）
  - 文件名全部小写，多个单词用下划线分隔。
  - 按照业务逻辑进行命名。  
      例：  
    `warning.ogg`  

> 注：如果是 html、js 等 web 资源文件，则相应的遵循 web 资源的命名规则。  





